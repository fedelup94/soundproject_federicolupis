// SoundProject_EXE.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Wrapper.h"
#include "common/common.h"

void DrawUI();
void CheckBtnPress();


Wrapper wrapperino(150);

const std::wstring pathSound1 = wrapperino.GetRootDir() + L"\\media\\bgm\\Cyberpunk_NCS.mp3";
const std::wstring pathSound2 = wrapperino.GetRootDir() + L"\\media\\effect\\hit_sound.mp3";
const std::wstring pathSound3 = wrapperino.GetRootDir() + L"\\media\\effect\\mlg_horn.mp3";
const std::wstring pathSound4 = wrapperino.GetRootDir() + L"\\media\\effect\\mlg_no_one_done_that.mp3";

Sound* sound1 = wrapperino.CreateSound(pathSound1);
Sound* sound2 = wrapperino.CreateSound(pathSound2);
Sound* sound3 = wrapperino.CreateSound(pathSound3);
Sound* sound4 = wrapperino.CreateStream(pathSound4);

std::vector<Sound*> vec{ sound2,sound3 };

const float mediumVolume = .65f;

float channelGroupVolume;


int main()
{

    wrapperino.InitChannel(wrapperino.GetChannel(0), sound1);
    wrapperino.InitChannel(wrapperino.GetChannel(1), sound3);
    wrapperino.InitChannel(wrapperino.GetChannel(11), sound2);
    wrapperino.InitChannel(wrapperino.GetChannel(12), sound3);


    wrapperino.CreateGroup(L"MAIN");

    Channel* temp1 = &wrapperino.GetChannel(0);
    temp1->StartPlay();

    wrapperino.AddChannelToGroup(wrapperino.GetChannel(11), wrapperino.GetChannelGroup(L"MAIN"));
    wrapperino.AddChannelToGroup(wrapperino.GetChannel(12), wrapperino.GetChannelGroup(L"MAIN"));

    wrapperino.GetChannelGroup(L"MAIN").DecreaseVolume(.35f);
    wrapperino.PlayOnChannelGroup(vec, L"MAIN", {SOUND_MODE::DEFAULT, SOUND_MODE::LOOP});
    channelGroupVolume = wrapperino.GetChannelGroup(L"MAIN").GetVolume();
    wrapperino.StopChannelGroup(L"MAIN");

    do
    {

        Common_Update();

        CheckBtnPress();

        DrawUI();


    }
    while (!Common_BtnPress(BTN_QUIT));
}

void DrawUI()
{
    Common_Draw("");
    Common_Draw("Federico Lupis");
    Common_Draw("FMOD Wrapper");
    Common_Draw("Utilizzando il tasto %s mettere in play o in pausa una canzone in sottofondo", Common_BtnStr(BTN_ACTION1));
    Common_Draw("Con %s, %s e %s far partire degli FX", Common_BtnStr(BTN_ACTION2), Common_BtnStr(BTN_ACTION3), Common_BtnStr(BTN_ACTION4));
    Common_Draw("Con %s sar� possibile mettere in play un channel group (in loop) di due suoni e con %s sar� possibile stopparlo.", Common_BtnStr(BTN_P), Common_BtnStr(BTN_S));
    Common_Draw("");
    Common_Draw("Press %s to toggle the BGM", Common_BtnStr(BTN_ACTION1));
    Common_Draw("Press %s to play once mlg_horn.mp3", Common_BtnStr(BTN_ACTION2));
    Common_Draw("Press %s to play once hit.mp3", Common_BtnStr(BTN_ACTION3));
    Common_Draw("Press %s to play once no one has ever done that.mp3", Common_BtnStr(BTN_ACTION4));
    Common_Draw("Press %s or %s to pan left or right the BGM", Common_BtnStr(BTN_LEFT), Common_BtnStr(BTN_RIGHT));
    Common_Draw("Press %s or %s to increase or decrease the volume of BGM", Common_BtnStr(BTN_UP), Common_BtnStr(BTN_DOWN));
    Common_Draw("Press %s to play ChannelGroup Main", Common_BtnStr(BTN_P));
    Common_Draw("Press %s to stop ChannelGroup Main", Common_BtnStr(BTN_S));

    Common_Draw("Press %s to quit", Common_BtnStr(BTN_QUIT));
    Common_Draw("");
    Common_Draw("");

}

void CheckBtnPress()
{

    //button1 (1 tastiera)
    if (Common_BtnPress(BTN_ACTION1))
    {
        wrapperino.GetChannel(0).Toggle();
    }

    //button2 (2 tastiera)
    if (Common_BtnPress(BTN_ACTION2))
    {
        wrapperino.GetChannel(1).SetSoundOneShot();
        wrapperino.PlayOneShotOnChannel(sound3, 1);
        wrapperino.GetChannel(1).SetVolume(mediumVolume);
        //wrapperino.GetChannel(1).IncreaseVolume(.5f);
    }

    //button3 (3 tastiera)
    if (Common_BtnPress(BTN_ACTION3))
    {
        wrapperino.GetChannel(2).SetSoundOneShot();
        wrapperino.PlayOneShotOnChannel(sound2, 2);
        wrapperino.GetChannel(2).SetVolume(mediumVolume);

    }

    //button4 (4 tastiera)
    if (Common_BtnPress(BTN_ACTION4))
    {
        wrapperino.GetChannel(3).SetSoundOneShot();
        wrapperino.PlayOneShotOnChannel(sound4, 3);
        wrapperino.GetChannel(3).SetVolume(mediumVolume);

        //wrapperino.GetChannel(3).SetVolume(20);
    }

    if (Common_BtnPress(BTN_S))
    {
        wrapperino.StopChannelGroup(L"MAIN");
    }

    if (Common_BtnPress(BTN_P))
    {
        wrapperino.GetChannelGroup(L"MAIN").SetVolume(channelGroupVolume);
        wrapperino.PlayOnChannelGroup(vec, L"MAIN", {SOUND_MODE::LOOP, SOUND_MODE::LOOP});
    }

    if (Common_BtnDown(BTN_UP))
    {
        wrapperino.GetChannel(0).IncreaseVolume();
    }

    if (Common_BtnDown(BTN_DOWN))
    {
        wrapperino.GetChannel(0).DecreaseVolume();
    }

    if (Common_BtnDown(BTN_LEFT))
    {
        wrapperino.GetChannel(0).PanLeft();
    }

    if (Common_BtnDown(BTN_RIGHT))
    {
        wrapperino.GetChannel(0).PanRight();
    }


}


