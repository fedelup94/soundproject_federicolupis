#pragma once

#include <iostream>
#include <stdio.h>
#include <fmod.hpp>
#include <Windows.h>
#include <vector>
#include <unordered_map>
#include "common/common.h"
#include "stdafx.h"
#include "Channel.h"
#include "ChannelGroup.h"

typedef FMOD::Sound Sound;
typedef FMOD::System System;

enum class SOUND_MODE
{
	DEFAULT,
	LOOP,
	LOOP_BIDI
};


class Wrapper 
{
public:


	System* GetSystem() { return system; }

	explicit Wrapper(unsigned const maxChannels = 100);
	virtual ~Wrapper();

	void IncreaseVolume(FMOD::ChannelGroup* group, float step = .1f);
	void DecreaseVolume(FMOD::ChannelGroup* group, float step = .1f);


	//Channel Functions
	void InitChannel(Channel& channel, Sound* sound);
	void Play(Sound* sound);
	void PlayOnChannel(Sound* sound, unsigned channel);
	void PlayChannel(unsigned channel);
	void PlayChannel(Channel& channel);
	void StopChannel(unsigned channel);
	void StopChannel(Channel& channel);
	void PlayLoopOnChannel(Sound* sound, unsigned channel);
	void PlayLoopOnChannel(Sound* sound, Channel& channel);
	void PlayOneShotOnChannel(Sound* sound, unsigned channel);
	void PlayOneShotOnChannel(Sound* sound, Channel& channel);

	//Group Functions
	void PlayOnChannelGroup(std::vector<Sound*> sound, const std::wstring& groupName, std::vector<SOUND_MODE>);
	void PlayOnChannelGroup(std::vector<Sound*> sound, ChannelGroup& group, std::vector<SOUND_MODE>);
	void PlayOnChannelGroup(std::vector<Sound*> sound, ChannelGroup& group);
	void PlayOnChannelGroup(std::vector<Sound*> sound,const std::wstring& groupName);
	void StopChannelGroup(const std::wstring& groupName);
	void StopChannelGroup(ChannelGroup& group);

	void AddChannelToGroup(Channel& channel, ChannelGroup& group);
	ChannelGroup& CreateGroup(const std::wstring& groupName);
	ChannelGroup& GetChannelGroup(const std::wstring& group);


	Sound* CreateSound(const std::wstring& path, FMOD_MODE mode = FMOD_DEFAULT);
	Sound* CreateSoundAsync(const std::wstring& path);
	Sound* CreateStream(const std::wstring& path);

	

	const Sound* GetSound(const std::wstring& path) { return (sounds.find(path) != sounds.end()) ? sounds.at(path) : NULL; }
	Channel& GetChannel(unsigned pos) { return channels.at(pos); }

	const std::wstring GetRootDir() const { return m_rootDir; }


private:
	System* system;
	std::wstring m_rootDir;
	std::unordered_map<std::wstring, Sound*> sounds;
	std::unordered_map<unsigned, float> channelsPanLevels;
	std::unordered_map<std::wstring, ChannelGroup> channelsGroups;

	std::vector<Channel> channels;
	void* extradriverdata = 0;

	static FMOD_MODE CONVERTMODE(SOUND_MODE);

	const char* ConvertWStringToChar (const std::wstring&);
	Channel& GetFirstEmpty();



	


};

