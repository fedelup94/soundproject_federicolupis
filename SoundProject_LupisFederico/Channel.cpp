#include "Channel.h"
#include "ChannelGroup.h"


Channel::Channel(const Channel& otherChannel)
{
	this->DeepCopy(otherChannel);
}

Channel& Channel::operator=(const Channel& otherChannel)
{
	this->DeepCopy(otherChannel);
	return *this;
}

Channel::Channel(Channel&& movedChannel) noexcept
{
	this->DeepSwap(movedChannel);
}

 Channel& Channel::operator=(Channel&& movedChannel) noexcept
 {
	 this->DeepSwap(movedChannel);
	 return *this;
 }

 void Channel::Init(Sound* sound, FMOD::System* system)
 {
	 isInit = true;
	 isEmpty = false;
	 this->sound = sound;
	 system->playSound(this->sound, 0, true, &channel);
 }

 void Channel::Unpause()
{
	if (!isPlaying && !isEmpty)
	{ 
		channel->setPaused(false);
		isPlaying = true;
	}
}

void Channel::Pause()
{
	if (isPlaying && !isEmpty)
	{
		channel->setPaused(true);
		isPlaying = false;
	}
}

void Channel::StartPlay()
{
	if (!isEmpty)
	{
		isPlaying = true;
		channel->setPaused(false);
	}
}

void Channel::Toggle()
{
	if (!isEmpty)
	{
		channel->setPaused(isPlaying);
		isPlaying = !isPlaying;
	}
}


void Channel::SetSoundLoop()
{
	sound->setMode(FMOD_LOOP_NORMAL);
	channel->setMode(FMOD_LOOP_NORMAL);
}

void Channel::SetSoundOneShot()
{
	sound->setMode(FMOD_LOOP_OFF);
	channel->setMode(FMOD_LOOP_OFF);
}

void Channel::SetVolume(const float value)
{
	volume = value;
	//volume = (volume <= MAX_CHANNEL_VOLUME) ? volume : MAX_CHANNEL_VOLUME;
	//volume = (volume >= -MAX_CHANNEL_VOLUME) ? volume : MAX_CHANNEL_VOLUME;
	channel->setVolume(volume);


}

void Channel::IncreaseVolume(const float step)
{

	volume += (step < 0) ? abs(step) : step;
	volume = (volume <= MAX_CHANNEL_VOLUME) ? volume : MAX_CHANNEL_VOLUME;
	channel->setVolume(volume);
}

void Channel::DecreaseVolume(const float step)
{
	volume -= (step < 0) ? abs(step) : step;
	volume = (volume <= 0) ? 0 : volume;
	channel->setVolume(volume);
}

void Channel::PanLeft(const float step)
{
	pan -= (step < 0) ? abs(step) : step;
	pan = (pan <= -1) ? -1 : pan;
	channel->setPan(pan);
}

void Channel::PanRight(const float step)
{
	pan += (step < 0) ? abs(step) : step;
	pan = (pan > 1) ? 1 : pan;
	channel->setPan(pan);
}

void Channel::LoadSound(Sound* sound, FMOD_MODE mode)
{
	if (sound != NULL)
	{
		this->sound = sound;
		this->sound->setMode(mode);
		isEmpty = false;
	}
}


void Channel::DeepCopy(const Channel& otherChannel)
{
	this->channel = otherChannel.channel;
	this->sound = otherChannel.sound;
	this->volume = otherChannel.volume;
	this->pan = otherChannel.pan;
	this->isPlaying = otherChannel.isPlaying;
	this->isEmpty = otherChannel.isEmpty;
	this->isInit = otherChannel.isInit;
}

void Channel::DeepSwap(Channel& movedChannel)
{
	this->channel = std::move(movedChannel.channel);
	this->sound = std::move(movedChannel.sound);
	this->volume = movedChannel.volume;
	this->pan = movedChannel.pan;
	this->isPlaying = movedChannel.isPlaying;
	this->isEmpty = movedChannel.isEmpty;
	this->isInit = movedChannel.isInit;

	movedChannel.channel = NULL;
	movedChannel.sound = NULL;
}




