#pragma once
#include <iostream>
#include <stdio.h>
#include <fmod.hpp>
#include <Windows.h>
#include <vector>
#include <unordered_map>
#include "common/common.h"
#include "stdafx.h"

typedef FMOD::Sound Sound;
class Channel
{
public:
	explicit Channel() : volume(1.f), pan(0), isPlaying(false), isEmpty(true), channel(), sound(), isInit(false) {}
	virtual ~Channel() { }
	Channel(const Channel&);
	Channel & operator=(const Channel&);
	Channel(Channel&&) noexcept;
	Channel& operator=(Channel&&) noexcept;
	
	bool operator==(const Channel& other) { return (this->channel == other.channel); }

	operator FMOD::Channel** () { return &channel; }

	void Init(Sound* sound, FMOD::System* system);

	void Unpause();
	void Pause();
	void Toggle();
	void SetSoundLoop();
	void SetSoundOneShot();
	void SetVolume(const float value);
	void IncreaseVolume(const float step = .1f);
	void DecreaseVolume(const float step = .1f);
	void PanLeft(const float step = .25f);
	void PanRight(const float step = .25f);

	void LoadSound(Sound* sound, FMOD_MODE mode = FMOD_DEFAULT);

	void StartPlay();


	const bool IsInit() { return isInit; }
	const bool IsPlaying() { return isPlaying; }
	const bool IsEmpty() { return isEmpty; }
	Sound* GetSound() { return sound; }
	FMOD::Channel* GetChannel() { return channel; }
	const float GetVolume() { return volume; }
	const float GetPan() { return pan; }


private:
	static constexpr float MAX_CHANNEL_VOLUME = 20.f;

	void DeepCopy(const Channel& other);
	void DeepSwap(Channel& other);

	FMOD::Channel* channel;
	Sound* sound;
	float volume;
	float pan;
	bool isPlaying;
	bool isEmpty;
	bool isInit;
};

