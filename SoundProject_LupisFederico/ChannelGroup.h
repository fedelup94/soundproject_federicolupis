#pragma once
#include "Channel.h"


class ChannelGroup
{
public:
	explicit ChannelGroup() : channelGroup(), channels(), name(L"NULL"), volume(1), isPlaying(false), pan(0) {}
	explicit ChannelGroup(const std::wstring& _name) : channelGroup(), channels(), name(_name), volume(1), isPlaying(false), pan(0) {}
	virtual ~ChannelGroup() { if (!channels.empty()) channelGroup->release(); }

	bool operator==(const ChannelGroup& other) { return name == other.name; }


	void Init(FMOD::ChannelGroup* group) { channelGroup = group; }
	void AddChannelToGroup(Channel& channel);
	void RemoveChannelFromGroup(Channel& channel);
	bool ContainsChannel(const Channel& channel);



	void SetVolume(const float _volume) { volume = _volume; }
	void IncreaseVolume(const float step = .1f);
	void DecreaseVolume(const float step = .1f);
	void PanLeft(const float step = .25f);
	void PanRight(const float step = .25f);
	void Pause();
	void UnPause();

	void Stop();
	
	unsigned GetNumChannels();
	const std::wstring& GetName() { return name; }
	Channel& GetChannel(const int index);
	std::vector<Channel*>& GetAllChannels();
	const bool IsPlaying() { return isPlaying; }
	const float GetVolume() { return volume; }

	operator FMOD::ChannelGroup* () { return channelGroup; }


private:
	static constexpr float MAX_CHANNEL_VOLUME = 20.f;
	std::wstring name;
	FMOD::ChannelGroup* channelGroup;
	std::vector<Channel*> channels;
	float volume;
	float pan;
	bool isPlaying;
};

