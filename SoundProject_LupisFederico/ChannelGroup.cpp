#include "ChannelGroup.h"

void ChannelGroup::AddChannelToGroup(Channel& channel)
{
	if (!ContainsChannel(channel))
	{
		channel.GetChannel()->setChannelGroup(channelGroup);
		channels.push_back(&channel);
	}
	else
		throw "Channel already in the group";

}

void ChannelGroup::RemoveChannelFromGroup(Channel& channel)
{
	if (ContainsChannel(channel))
	{
		channel.GetChannel()->setChannelGroup(NULL);
		std::vector<Channel*>::iterator it = channels.begin();
		while (it != channels.end())
		{
			if (**it == channel)
			{
				channels.erase(it);
				return;
			}
		}
	}
	else
		throw "Can't find the Group";
}




bool ChannelGroup::ContainsChannel(const Channel& channel)
{
	for (auto elem : channels)
		if (*elem == channel)
		{
			return true;
		}
	return false;
}

void ChannelGroup::IncreaseVolume(const float step)
{
	volume += (step < 0) ? abs(step) : step;
	volume = (volume <= MAX_CHANNEL_VOLUME) ? volume : MAX_CHANNEL_VOLUME;
	channelGroup->setVolume(volume);
}

void ChannelGroup::DecreaseVolume(const float step)
{
	volume -= (step < 0) ? abs(step) : step;
	volume = (volume <= 0) ? 0 : volume;
	channelGroup->setVolume(volume);
}

void ChannelGroup::PanLeft(const float step)
{
	pan -= (step < 0) ? abs(step) : step;
	pan = (pan <= -1) ? -1 : pan;
	channelGroup->setPan(pan);
}

void ChannelGroup::PanRight(const float step)
{
	pan += (step < 0) ? abs(step) : step;
	pan = (pan > 1) ? 1 : pan;
	channelGroup->setPan(pan);
}

void ChannelGroup::Pause()
{
	if (isPlaying)
	{
		isPlaying = false;
		channelGroup->setPaused(true);
	}
}

void ChannelGroup::UnPause()
{
	if (!isPlaying)
	{
		isPlaying = true;
		channelGroup->setPaused(false);
	}
}

void ChannelGroup::Stop()
{
	channelGroup->stop();
}

unsigned ChannelGroup::GetNumChannels()
{
	return channels.size();
}

std::vector<Channel*>& ChannelGroup::GetAllChannels()
{
	return channels;
}

Channel& ChannelGroup::GetChannel(const int indexToFind)
{
	int index = 0;
	for (auto channel : channels)
	{
		channel->GetChannel()->getIndex(&index);
		if (index == indexToFind)
			return *channel;
	}
	throw "Can't find the channel in the Group";
}
