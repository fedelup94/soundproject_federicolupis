#include "Wrapper.h"


Wrapper::Wrapper(unsigned const maxChannels) : sounds(), channels(maxChannels), channelsPanLevels(maxChannels), channelsGroups()
{

	ERRCHECK(FMOD::System_Create(&system));

	ERRCHECK(system->init(maxChannels, FMOD_INIT_NORMAL, 0));

	for (unsigned i = 0; i < maxChannels; i++)
	{
		channelsPanLevels[i] = 0.f;
	}


	Common_Init(&extradriverdata);



	std::unique_ptr<wchar_t[]> buffer(new wchar_t[MAX_PATH]);
	GetModuleFileNameW(nullptr, buffer.get(), MAX_PATH);
	std::wstring completePath(buffer.get());
	m_rootDir = completePath.erase(completePath.find_last_of(L"\\/"));


}

Wrapper::~Wrapper()
{

	for (auto& sound : sounds)
		ERRCHECK(sound.second->release());
	for (auto& group : channelsGroups)
		group.second.~ChannelGroup();

	ERRCHECK(system->close());

	ERRCHECK(system->release());

	Common_Close();

}

Sound* Wrapper::CreateSoundAsync(const std::wstring& path)
{
	return CreateSound(path, FMOD_NONBLOCKING);
}

Sound* Wrapper::CreateStream(const std::wstring& path)
{
	return CreateSound(path, FMOD_CREATESTREAM);
}

Sound* Wrapper::CreateSound(const std::wstring& path, FMOD_MODE mode)
{
	Sound* sound = NULL;
	const char* newPath = ConvertWStringToChar(path);


	if (sounds.find(path) == sounds.end())
	{
		ERRCHECK(system->createSound(newPath, mode, 0, &sound));
		sounds[path] = sound;
	}

	delete newPath;
	return sound;

}

ChannelGroup& Wrapper::CreateGroup(const std::wstring& groupName)
{
	FMOD::ChannelGroup* group = NULL;
	const char* newGroupName = ConvertWStringToChar(groupName);

	if (channelsGroups.find(groupName) == channelsGroups.end())
	{
		ERRCHECK(system->createChannelGroup(newGroupName, &group));
		ChannelGroup* newGroup = &ChannelGroup(groupName);
		newGroup->Init(group);
		channelsGroups[groupName] = *newGroup;
		delete newGroupName;
		return *newGroup;

	}
	throw "A channel with that name already exists";

}


Channel& Wrapper::GetFirstEmpty()
{
	for (unsigned i = 0; i < channels.size(); ++i)
	{
		if (channels[i].IsEmpty())
		{
			return channels[i];
		}
	}
	throw "No empty Channels Available";
}

void Wrapper::Play(Sound* sound)
{
	auto channel = GetFirstEmpty();
	if (!channel.IsInit())
		channel.Init(sound, system);
	else
		channel.LoadSound(sound);

	channel.LoadSound(sound);
	channel.StartPlay();
	ERRCHECK(system->playSound(sound, 0, false, channel));
}

void Wrapper::PlayOnChannel(Sound* sound, unsigned channel)
{
	if (!channels[channel].IsInit())
		channels[channel].Init(sound,system);
	else
		channels[channel].LoadSound(sound);

	channels[channel].StartPlay();
	ERRCHECK(system->playSound(sound, 0, false, channels[channel]));
}

void Wrapper::PlayChannel(unsigned channel)
{
	if (channel < channels.size())
		PlayChannel(channels[channel]);


}

void Wrapper::PlayChannel(Channel& channel)
{
	if (!channel.IsPlaying())
	{
		if (!channel.IsInit())
		{
			channel.StartPlay();
			ERRCHECK(system->playSound(channel.GetSound(), 0, false, channel));
		}
	}

}

void Wrapper::StopChannel(unsigned channel)
{
	if (!channels[channel].IsEmpty())
	{
		channels[channel].Pause();
		Channel newChannel = channels[channel];
		int index;
		channels[channel].GetChannel()->getIndex(&index);
		channels[channel].GetChannel()->stop();
		Wrapper::GetSystem()->getChannel(index, newChannel);
		newChannel.Init(newChannel.GetSound(), Wrapper::GetSystem());
		channels[channel] = newChannel;
	}
}

void Wrapper::StopChannel(Channel& channel)
{
	if (!channel.IsEmpty())
	{
		channel.Pause();
		Channel newChannel = channel;
		int index;
		channel.GetChannel()->getIndex(&index);
		channel.GetChannel()->stop();
		Wrapper::GetSystem()->getChannel(index, newChannel);
		newChannel.Init(newChannel.GetSound(), Wrapper::GetSystem());
		channel = newChannel;
	}
}

void Wrapper::PlayLoopOnChannel(Sound* sound, unsigned channel)
{
	if (channel < channels.size())
	PlayLoopOnChannel(sound, channels[channel]);
}

void Wrapper::PlayLoopOnChannel(Sound* sound, Channel& channel)
{
	if (!channel.IsInit())
		channel.Init(sound, system);
	else
		channel.LoadSound(sound);

	channel.SetSoundLoop();
	channel.StartPlay();	

	FMOD::ChannelGroup* group;
	channel.GetChannel()->getChannelGroup(&group);

	ERRCHECK(system->playSound(channel.GetSound(), group, false, channel));
}

void Wrapper::PlayOneShotOnChannel(Sound* sound, unsigned channel)
{
	if (channel < channels.size())
		PlayOneShotOnChannel(sound, channels[channel]);
}

void Wrapper::PlayOneShotOnChannel(Sound* sound, Channel& channel)
{
	if (!channel.IsInit())
		channel.Init(sound, system);
	else
		channel.LoadSound(sound);

	channel.SetSoundOneShot();
	channel.StartPlay();

	FMOD::ChannelGroup* group;
	channel.GetChannel()->getChannelGroup(&group);

	ERRCHECK(system->playSound(channel.GetSound(), group, false, channel));
}

void Wrapper::PlayOnChannelGroup(std::vector<Sound*> sound, const std::wstring& groupName, std::vector<SOUND_MODE> modes)
{
	if (channelsGroups.find(groupName) != channelsGroups.end())
	{
		PlayOnChannelGroup(sound, channelsGroups.at(groupName), modes);
	}
}

void Wrapper::PlayOnChannelGroup(std::vector<Sound*> sound, ChannelGroup& group, std::vector<SOUND_MODE> modes)
{
	if (sound.size() <= group.GetAllChannels().size())
	{
		for (unsigned i = 0; i < sound.size(); ++i)
		{
			auto channel = group.GetAllChannels()[i];
			sound[i]->setMode(CONVERTMODE(modes[i]));

			if (!channel->IsInit())
				channel->Init(sound[i], system);
			else
				channel->LoadSound(sound[i]);

			if (channel->IsPlaying())
				channel->Pause();

			channel->GetChannel()->setMode(CONVERTMODE(modes[i]));
			channel->StartPlay();
			//ERRCHECK(system->playSound(sound[i], group, false, *channel));

		}
	}
	else
	{
		for (unsigned i = 0; i < group.GetAllChannels().size(); ++i)
		{
			auto channel = group.GetAllChannels()[i];
			sound[i]->setMode(CONVERTMODE(modes[i]));

			if (!channel->IsInit())
				channel->Init(sound[i], system);
			else
				channel->LoadSound(sound[i]);

			channel->GetChannel()->setMode(CONVERTMODE(modes[i]));
			channel->StartPlay();
			ERRCHECK(system->playSound(sound[i], group, false, *channel));

		}
	}
}

void Wrapper::PlayOnChannelGroup(std::vector<Sound*> sound, ChannelGroup& group)
{
	std::vector<SOUND_MODE> modes;
	for (unsigned i = 0; sound.size(); ++i)
		modes.push_back(SOUND_MODE::DEFAULT);
	PlayOnChannelGroup(sound, group, modes);
	
}

void Wrapper::PlayOnChannelGroup(std::vector<Sound*> sound, const std::wstring& groupName)
{
	if (channelsGroups.find(groupName) != channelsGroups.end())
	{
		std::vector<SOUND_MODE> modes;
		for (unsigned i = 0; i < sound.size();++i)
			modes.push_back(SOUND_MODE::DEFAULT);
		PlayOnChannelGroup(sound, channelsGroups.at(groupName), modes);
	}
}

void Wrapper::StopChannelGroup(const std::wstring& groupName)
{
	if (channelsGroups.find(groupName) != channelsGroups.end())
	{
		StopChannelGroup(channelsGroups.at(groupName));
	}
}

void Wrapper::StopChannelGroup(ChannelGroup& group)
{
	group.Stop();
	for (auto it : group.GetAllChannels())
		StopChannel(*it);
	for (auto it : group.GetAllChannels())
	{
		it->Init(it->GetSound(), system);
	}
}

void Wrapper::IncreaseVolume(FMOD::ChannelGroup* group, float step)
{
	float vol = 0.f;
	ERRCHECK(group->getVolume(&vol));
	vol = vol >= 1 ? 1 : vol + step;
	ERRCHECK(group->setVolume(vol));
}

void Wrapper::DecreaseVolume(FMOD::ChannelGroup* group, float step)
{
	float vol = 0.f;
	ERRCHECK(group->getVolume(&vol));
	vol = vol <= 0 ? 0 : vol - step;
	ERRCHECK(group->setVolume(vol));
}

void Wrapper::InitChannel(Channel& channel, Sound* sound)
{
	if (!channel.IsInit())
		channel.Init(sound, system);
	else
		channel.LoadSound(sound);
}


void Wrapper::AddChannelToGroup(Channel& channel, ChannelGroup& group)
{
	if (channel.IsInit())
	{
		group.AddChannelToGroup(channel);		
	}
}

ChannelGroup& Wrapper::GetChannelGroup(const std::wstring& group)
{
	{ return (channelsGroups.find(group) != channelsGroups.end()) ? channelsGroups.at(group) : throw "Can't find the channelgroup"; }
}

FMOD_MODE Wrapper::CONVERTMODE(SOUND_MODE mode)
{
	switch (mode)
	{
	case (SOUND_MODE::DEFAULT):		return FMOD_DEFAULT;
	case (SOUND_MODE::LOOP):		return FMOD_LOOP_NORMAL;
	case (SOUND_MODE::LOOP_BIDI):	return FMOD_LOOP_BIDI;
	default: throw "Unexpected Sound_Mode";
	}
}

const char* Wrapper::ConvertWStringToChar(const std::wstring& src)
{
	auto wcharSrc = src.c_str();
	char* dest = new char[1024];

	size_t i;

	wcstombs_s(&i, dest, sizeof(char)*1024, wcharSrc, (size_t)1024);

	return dest;
}